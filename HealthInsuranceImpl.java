package com.emids.insurance;

import com.emids.person.CurrentHealth;
import com.emids.person.Habbit;
import com.emids.person.PersonInfo;

public class HealthInsuranceImpl implements IHealthInsurance{

	int minimumPremium=5000;
	float cHealth=0;
	float cHabbit=0;
	
	public int calculatePremium(PersonInfo pInfo) {
		int primium=0;
		int age=pInfo.getAge();
		if(age<18) {
			primium=minimumPremium;
			return primium;
		}else if(age>=18 && age<25) {
			float per=0.1f;
			boolean cStatus=this.checkPreCondition(pInfo.getcHealth());
			boolean hStatus=this.checkHabit(pInfo.getHabbits());
			if(cStatus) {
				cHealth=0.1f;
			}if(hStatus) {
				cHabbit=0.03f;
			}
			primium=this.calculate(per, pInfo.getGender(),cHealth,cHabbit);
			
		}else if(age>=25 && age<30) {
			boolean cStatus=this.checkPreCondition(pInfo.getcHealth());;
			boolean hStatus=this.checkHabit(pInfo.getHabbits());
			if(cStatus) {
				cHealth=0.01f;
			}if(hStatus) {
				cHabbit=0.03f;
			}
			primium=this.calculate(0.1f, pInfo.getGender(),cHealth,cHabbit);
		}else if (age>=30 && age<35) {
			boolean cStatus=this.checkPreCondition(pInfo.getcHealth());;
			boolean hStatus=this.checkHabit(pInfo.getHabbits());
			if(cStatus) {
				cHealth=0.01f;
			}if(hStatus) {
				cHabbit=0.03f;
			}
				primium=this.calculate(0.1f, pInfo.getGender(),cHealth,cHabbit);
		}else if (age>=35 && age<40) {
			boolean cStatus=this.checkPreCondition(pInfo.getcHealth());;
			boolean hStatus=this.checkHabit(pInfo.getHabbits());
			if(cStatus) {
				cHealth=0.01f;
			}if(hStatus) {
				cHabbit=0.03f;
			}
			primium=this.calculate(0.1f, pInfo.getGender(),cHealth,cHabbit);
		
		}else {
		
			boolean cStatus=this.checkPreCondition(pInfo.getcHealth());;
			boolean hStatus=this.checkHabit(pInfo.getHabbits());
			if(cStatus) {
				cHealth=0.01f;
			}if(hStatus) {
				cHabbit=0.03f;
			}
			primium=this.calculate(0.2f, pInfo.getGender(),cHealth,cHabbit);
		}
		
		return primium;
	}
	
	private boolean checkHabit(Habbit habbits) {
		boolean hStatus=false;
		if(habbits.isAlchohal()) {
			hStatus=true;
		}else if (habbits.isDrugs()) {
			hStatus=true;
		}else if(habbits.isExserise()){
			hStatus=true;
		}else if (habbits.isSmoking()) {
			hStatus=true;
		}
		return hStatus;
	}

	private boolean checkPreCondition(CurrentHealth cHealth) {
		boolean status=false;
		if (cHealth.isBp()) {
			status=true;
		}else if (cHealth.isBs()) {
			status=true;
		}else if (cHealth.isHt()) {
			status=true;
		}else if(cHealth.isOw()){
			status=true;
			
		}
		return status;
	}

	private int calculate(float precentage,String gender,float cHealth,float cHabbit) {
		int ac=0;
		ac=(int) (minimumPremium+(precentage*minimumPremium));
		//ac=ac+minimumPremium;
		if(gender.equals("m")) {
			ac=(int) (ac+(0.02*ac));
		}
		ac=(int) (ac+cHealth*ac);
		ac=(int) (ac+cHabbit*ac);
		return ac;
	}

}
