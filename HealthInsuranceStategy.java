package com.emids.pattern;

import com.emids.insurance.IHealthInsurance;
import com.emids.person.PersonInfo;

public class HealthInsurancStrategy {
	public IHealthInsurance healthInsurance;

	public HealthInsurancStrategy(IHealthInsurance insurance) {
		this.healthInsurance = insurance;
	}

	public int calculate(PersonInfo pInfo) {
		return healthInsurance.calculatePremium(pInfo);
	}

}
