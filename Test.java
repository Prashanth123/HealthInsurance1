package com.emids.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//import com.emids.insurance.HealthInsurance;
import com.emids.insurance.HealthInsuranceImpl;
import com.emids.insurance.IHealthInsurance;
import com.emids.pattern.HealthInsurancStrategy;
import com.emids.person.CurrentHealth;
import com.emids.person.Habbit;
import com.emids.person.PersonInfo;

public class Test {
	public static void main(String[] args) throws IOException {
		System.out.println("Enter Name:");
		BufferedReader bf=new BufferedReader(new InputStreamReader(System.in));
		String name=bf.readLine();
		System.out.println("Enter Gender");
		String gender=bf.readLine();
		System.out.println("Enter Age");
		int age=Integer.parseInt(bf.readLine());
		System.out.println("Enter Current Heath status");
		System.out.println("Enter Hypertension: yes/no");
		String ht=bf.readLine();
		boolean htStatus=true;
		if(ht.equals("no")) {
			htStatus=false;
		}
		System.out.println("Enter Bp: yes/no");
		String bp=bf.readLine();
		boolean bpStatus=true;
		if(bp.equals("no")) {
			bpStatus=false;
		}
		System.out.println("Enter Blood suger: yes/no");
		String bs=bf.readLine();
		boolean bsStatus=true;
		if(bs.equals("no")) {
			bsStatus=false;
		}
		System.out.println("Enter OverWeight: yes/no");
		String ow=bf.readLine();
		
		boolean owStatus=true;
		if(ow.equals("no")) {
			owStatus=false;
		}
		System.out.println("Enter Habbits: ");
		System.out.println("Enter alchohal: yes/no");
		
		String al=bf.readLine();
		boolean alStatus=true;
		if(al.equals("no")) {
			alStatus=false;
		}
		System.out.println("Enter smoking: yes/no");
		String sm=bf.readLine();
		boolean smoking=true;
		if(al.equals("no")) {
			smoking=false;
		}
		
		System.out.println("Enter Exsersize: yes/no");
		String ex=bf.readLine();
		
		boolean exserisest=true;
		if(al.equals("no")) {
			exserisest=false;
		}
		System.out.println("Enter Drugs: yes/no");
		String dr=bf.readLine();
		boolean drugst=true;
		if(al.equals("no")) {
			drugst=false;
		}
		
		
		PersonInfo pInfo=new PersonInfo();
		pInfo.setName(name);
		pInfo.setGender(gender);
		pInfo.setAge(age);
		CurrentHealth cHealth=new CurrentHealth();
		cHealth.setBp(bpStatus);
		cHealth.setBs(bsStatus);
		cHealth.setHt(htStatus);
		cHealth.setOw(owStatus);
		pInfo.setcHealth(cHealth);
		Habbit habbits=new Habbit();
		habbits.setAlchohal(alStatus);
		habbits.setDrugs(drugst);
		habbits.setExserise(exserisest);
		habbits.setSmoking(smoking);
		pInfo.setHabbits(habbits);
		
		//System.out.println(pInfo);
		IHealthInsurance his=new HealthInsuranceImpl();
		HealthInsurancStrategy strategy=new HealthInsurancStrategy(his);
		int premium=strategy.calculate(pInfo);
		System.out.println("Health Insurance Premium for"+pInfo.getName()+" :rs"+premium);;
	}

}
