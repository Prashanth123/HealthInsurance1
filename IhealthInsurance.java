package com.emids.insurance;

import com.emids.person.PersonInfo;

public interface IHealthInsurance {
	public int calculatePremium(PersonInfo pInfo);
}
